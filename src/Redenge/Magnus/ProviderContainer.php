<?php

namespace Redenge\Magnus;

use Redenge\Magnus\Exceptions\OutOfBoundsException;

class ProviderContainer
{
	/** @var \Redenge\Magnus\Provider[]  */
	private $providers = [];

	/**
	 * @param \Redenge\Magnus\Provider[] $providers
	 */
	public function __construct(array $providers)
	{
		foreach ($providers as $provider) {
			$this->addProvider($provider);
		}
	}

	/**
	 * @param \Redenge\Magnus\Provider $provider
	 *
	 * @return void
	 */
	private function addProvider(Provider $provider)
	{
		$this->providers[] = $provider;
	}

	/**
	 * @param \Redenge\Magnus\ProviderKey $key
	 *
	 * @return \Redenge\Magnus\Provider
	 * @throws \Redenge\Magnus\Exceptions\OutOfBoundsException
	 */
	public function getProvider(ProviderKey $key)
	{
		$matched = array_filter($this->providers, function (Provider $provider) use ($key) {
			return $provider->matchKey($key);
		});
		if (count($matched)) {
			return $matched[array_keys($matched)[0]];
		}

		throw new OutOfBoundsException('Provider with key ' . (string) $key . ' is not defined.');
	}
}
