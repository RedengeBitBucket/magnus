<?php

namespace Redenge\Magnus\Exceptions;

class OutOfBoundsException extends MagnusException
{
}
