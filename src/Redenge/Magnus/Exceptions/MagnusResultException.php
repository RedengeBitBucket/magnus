<?php

namespace Redenge\Magnus\Exceptions;

use Exception;


class MagnusResultException extends MagnusException
{

	/**
	 * @var array
	 */
	private $codes = [
		'KO' => 'Transakciu sa nepodarilo zapísať, pravdepodobne toto ID už existuje',
		'ZLA IP' => 'IP adresa z ktorej požiadavka prišla nesúhlasí s nastaveniami v systéme Magnus',
		'SHOP NEMA POVOLENU ZIADNU IP' => 'V rámci systému Magnus nemá daný partner definovanú povolenú IP adresu',
		'NEZNAMA CHYBA' => 'Iná chyba spracovania požiadavky',
	];

	/**
	 * @var string
	 */
	private $codeMagnus;


	/**
	 * @param string $message
	 * @param string $codeMagnus
	 * @param int $code
	 * @param \Exception $previous
	 */
	public function __construct($codeMagnus, $code = NULL, Exception $previous = NULL)
	{
		$this->codeMagnus = $codeMagnus;
		$message = $this->translate();

		parent::__construct($message, $code, $previous);
	}


	/**
	 * @return string
	 */
	public function getCodeMagnus()
	{
		return $this->codeMagnus;
	}


	/**
	 * @return string
	 */
	public function translate()
	{
		if (!isset($this->codes[$this->codeMagnus])) {
			return 'Unsupported translate for exception';
		}

		return $this->codes[$this->codeMagnus];
	}

}
