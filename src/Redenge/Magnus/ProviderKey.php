<?php

namespace Redenge\Magnus;

use Nette\Utils\Strings;
use Redenge\Magnus\Exceptions;

class ProviderKey
{
	/** @var string  */
	private $key;

	/**
	 * @param array ...$args
	 */
	public function __construct(...$args)
	{
		$this->key = implode('|', array_map(function ($arg) {
			if (is_scalar($arg)) {
				return Strings::lower((string) $arg);
			}
			throw new Exceptions\InvalidArgumentException("Argument must be scalar.");
		}, $args));
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->key;
	}
}
