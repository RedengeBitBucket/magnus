<?php

namespace Redenge\Magnus;

class ProviderKeyFactory
{
	/**
	 * @param string $multishopCode
	 * @param string $countryCode
	 *
	 * @return \Redenge\Magnus\ProviderKey
	 */
	public static function create($multishopCode, $countryCode)
	{
		return new ProviderKey($multishopCode, $countryCode);
	}
}
