<?php

namespace Redenge\Magnus\Components;

use Nette\Application\UI;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\ComponentModel\IContainer;
use Redenge\Magnus\Exceptions\MagnusException;
use Redenge\Magnus\Operation;
use Redenge\Magnus\Provider;


class MagnusControl extends UI\Control
{

	/**
	 * @var array of callbacks: function(MagnusControl $control, Request $request, Transaction $transaction)
	 */
	public $onCheckout = [];

	/**
	 * @var array of callbacks: function(MagnusControl $control, Response $response)
	 */
	public $onSuccess = [];

	/**
	 * @var array of callbacks: function(MagnusControl $control, \Exception $exception)
	 */
	public $onError = [];

	/**
	 * @var Operation
	 */
	private $operation;

	/**
	 * @var Provider
	 */
	private $provider;

	/**
	 * @var string
	 */
	private $templateFile;


	/**
	 * @param Operation $operation
	 * @param Provider $provider
	 * @param IContainer|null $control
	 * @param string $name
	 */
	public function __construct(Operation $operation, Provider $provider, IContainer $control = NULL, $name = NULL)
	{
		parent::__construct($control, $name);

		$this->operation = $operation;
		$this->provider = $provider;
	}


	/**
	 * @throws MagnusException
	 * @throws \Exception
	 */
	public function handleCheckout()
	{
		try {
			$this->provider->createTransaction($this->operation)->sendTransaction();

			$url = $this->provider->createRequest($this->operation)->getRequestUrl();
			$this->onCheckout($this, $this->provider->getRequest(), $this->provider->getTransaction());
			$this->getPresenter()->redirectUrl($url);

		} catch (MagnusException $e) {
			$this->errorHandler($e);
			return;
		}
	}


	/**
	 * @throws \Redenge\Magnus\Exceptions\MagnusException
	 */
	public function handleSuccess()
	{
		$params = $this->getPresenter()->getParameters();

		try {
			/** @var Response $response */
			$response = $this->provider->createResponse($params);
		} catch (MagnusException $e) {
			$this->errorHandler($e);
			return;
		}

		$this->onSuccess($this, $response);
	}


	/**
	 * @param MagnusException $exception
	 * @throws MagnusException
	 */
	protected function errorHandler(MagnusException $exception)
	{
		if (!$this->onError) {
			throw $exception;
		}

		$this->onError($this, $exception);
	}


	/**
	 * @param string $templateFile
	 */
	public function setTemplateFile($templateFile)
	{
		$this->templateFile = $templateFile;
	}


	/**
	 * @param array $attrs
	 * @param string $text
	 */
	public function render($attrs = [], $text = "Pay")
	{
		/** @var Template $template */
		$template = $this->getTemplate();
		$template->setFile($this->getTemplateFilePath());
		$template->add('checkoutLink', $this->link('//checkout!'));
		$template->add('text', $text);
		$template->add('attrs', $attrs);
		$template->render();
	}


	/**
	 * @return string
	 */
	public function getTemplateFilePath()
	{
		return ($this->templateFile)
			? $this->templateFile
			: $this->getDefaultTemplateFilePath();
	}


	/**
	 * @return string
	 */
	public function getDefaultTemplateFilePath()
	{
		return __DIR__ . DIRECTORY_SEPARATOR . 'templates/magnusControl.latte';
	}

}
