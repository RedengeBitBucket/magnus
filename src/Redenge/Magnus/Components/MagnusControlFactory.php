<?php

namespace Redenge\Magnus\Components;

use Nette\ComponentModel\IContainer;
use Redenge\Magnus\Operation;
use Redenge\Magnus\Provider;
use Redenge\Magnus\ProviderContainer;
use Redenge\Magnus\ProviderKey;


class MagnusControlFactory
{
	/** @var \Redenge\Magnus\ProviderContainer  */
	private $providerContainer;


	/**
	 * @param Provider $provider
	 */
	public function __construct(ProviderContainer $providerContainer)
	{
		$this->providerContainer = $providerContainer;
	}

	/**
	 * @param Operation $operation
	 * @return GPWebPayControl
	 */
	public function create(Operation $operation, ProviderKey $key, IContainer $control = NULL, $name = NULL)
	{
		return new MagnusControl($operation, $this->providerContainer->getProvider($key), $control, $name);
	}
}
