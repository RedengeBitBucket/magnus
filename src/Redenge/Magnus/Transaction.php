<?php

namespace Redenge\Magnus;


class Transaction
{

	/**
	 * @var Operation
	 */
	private $operation;

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var string
	 */
	private $transactionId;

	/**
	 * @var array
	 */
	private $params;


	/**
	 *
	 * @param \Redenge\Magnus\Operation $operation
	 * @param \Redenge\Magnus\Settings $settings
	 */
	public function __construct(Operation $operation, Settings $settings)
	{
		$this->operation = $operation;
		$this->settings = $settings;
		$this->transactionId = $this->generateTransactionId();
		bdump($this->transactionId);
		$this->setParams();
	}


	/**
	 * @return string
	 */
	private function generateTransactionId()
	{
		return $this->operation->getVs() . time() . uniqid(mt_rand(), true);
	}


	private function setParams()
	{
		$this->params['transId'] = $this->transactionId;
		$this->params['shopId'] = $this->settings->getShopId();
		$this->params['vs'] = $this->operation->getVs();
		$this->params['sum'] = $this->operation->getPointsAmount(
			$this->operation->getAmount(),
			$this->settings->getPointConversion($this->operation->getCurrency())
		);
	}


	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}


	/**
	 * @return string
	 */
	public function getTransactionId()
	{
		return $this->transactionId;
	}

}
