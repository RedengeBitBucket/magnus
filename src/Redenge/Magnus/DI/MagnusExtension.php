<?php

namespace Redenge\Magnus\DI;

use Nette;
use Nette\Utils\Validators;
use Redenge\Magnus\Components\MagnusControlFactory;
use Redenge\Magnus\Provider;
use Redenge\Magnus\ProviderContainer;
use Nette\DI\Statement;
use Redenge\Magnus\ProviderKey;
use Redenge\Magnus\Settings;


class MagnusExtension extends Nette\DI\CompilerExtension
{

	/**
	 * @var array
	 */
	public $defaults = [
		'settings' => [],
		'pointsConversion' => [
			'czk' => '4',
			'eur' => '100',
		],
	];


	/**
	 * return void
	 */
	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);

		$builder = $this->getContainerBuilder();

		$providers = [];
		foreach ($config['settings'] as $settingArr) {
			Validators::assertField($settingArr, 'key');
			Validators::assertField($settingArr, 'shopId');
			Validators::assertField($settingArr, 'production');

			$providers[] = new Statement(Provider::class, [
				'settings' => new Statement(Settings::class, [
					'shopId' => $settingArr['shopId'],
					'production' => $settingArr['production'],
					'pointsConversion' => $config['pointsConversion'],
				]),
				'key' => (!($key = $settingArr['key']) instanceof Statement) ? new Statement(ProviderKey::class, [$key]) : $key,
			]);
		}

		$builder->addDefinition($this->prefix('providerContainer'))
			->setClass(ProviderContainer::class)
			->setArguments([
				'providers' => $providers,
			]);

		$builder->addDefinition($this->prefix('controlFactory'))
			->setClass(MagnusControlFactory::class, [$this->prefix('@providerContainer')]);
	}


	/**
	 * @param Nette\Configurator $configurator
	 */
	public static function register(Nette\Configurator $configurator)
	{
		$configurator->onCompile[] = function ($config, Nette\DI\Compiler $compiler) {
			$compiler->addExtension('magnus', new MagnusExtension);
		};
	}

}
