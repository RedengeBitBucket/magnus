<?php

namespace Redenge\Magnus;


class Request
{

	/**
	 * @var string
	 */
	private $transactionId;

	/**
	 * @var string
	 */
	private $shopId;

	/**
	 * @var array
	 */
	private $params;


	/**
	 * @param string $transactionId
	 * @param string $shopId
	 */
	public function __construct($transactionId, $shopId)
	{
		$this->transactionId = $transactionId;
		$this->shopId = $shopId;

		$this->setParams();
	}


	/**
	 * Sets params to array
	 */
	private function setParams()
	{
		$this->params['transId'] = $this->transactionId;
		$this->params['shopId'] = $this->shopId;
	}


	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}

}
