<?php

namespace Redenge\Magnus;

use Exception;
use GuzzleHttp\Client;
use Redenge\Magnus\Exceptions\MagnusResultException;
use Redenge\Magnus\Request;


class Provider
{
	private $key;

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var Client
	 */
	private $client;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var Transaction
	 */
	private $transaction;


	/**
	 * @param Settings $settings
	 */
	public function __construct(Settings $settings, ProviderKey $key)
	{
		$this->key = $key;
		$this->settings = $settings;
		$this->client = new Client;
	}

	/**
	 * @param \Redenge\Magnus\ProviderKey $key
	 *
	 * @return bool
	 */
	public function matchKey(ProviderKey $key)
	{
		return (string) $key === (string) $this->key;
	}

	/**
	 * @param Operation $operation
	 * @return $this
	 */
	public function createRequest(Operation $operation)
	{
		$this->request = new Request(
			$this->transaction->getTransactionId(),
			$this->settings->getShopId()
		);

		return $this;
	}


	public function createTransaction(Operation $operation)
	{
		$this->transaction = new Transaction($operation, $this->settings);

		return $this;
	}


	/**
	 * @return Request
	 */
	public function getRequest()
	{
		return $this->request;
	}


	/**
	 * @return Transaction
	 */
	public function getTransaction()
	{
		return $this->transaction;
	}


	/**
	 * @return string
	 */
	public function getRequestUrl()
	{
		$paymentUrl = $this->settings->getUrl() . '/WPVSPayment/doPayment.xhtml?' . http_build_query($this->request->getParams());

		return $paymentUrl;
	}


	public function sendTransaction()
	{
		$uri = '/WPVSPayment/CreateTransaction';
		$params = $this->transaction->getParams();
		$return = $this->sendRequest($uri, $params);
		if ($return !== 'OK') {
			throw new MagnusResultException($return);
		}
	}


	public function checkPayment($transactionId)
	{
		$uri = '/WPVSPayment/CheckPayment';
		$params = [
			'transId' => $transactionId,
			'shopId' => $this->settings->getShopId(),
		];
		return $this->sendRequest($uri, $params);
	}


	/**
	 * @param string $uri
	 * @param type $params
	 * @param type $method
	 * @throws Exception
	 * @throws MagnusResultException
	 */
	private function sendRequest($uri, $params, $method = 'GET')
	{
		$url = $this->settings->getUrl() . $uri;
		$options = [
			'query' => $params,
			'verify' => false
		];

		$response = $this->client->request($method, $url, $options);
		if ($response->getStatusCode() === 400) {
			throw new Exception(sprintf('%d - Bad Request', $response->getStatusCode()));
		} elseif ($response->getStatusCode() === 404) {
			throw new Exception(sprintf('%d - Not Found', $response->getStatusCode()));
		} elseif ($response->getStatusCode() !== 200) {
			throw new Exception(sprintf('%s Error, HTTP Status Code = %d', get_class($this), $response->getStatusCode()));
		}

		return trim((string) $response->getBody());
	}


	/**
	 * @param $params
	 * @return Response
	 */
	public function createResponse($params)
	{
		$transactionId = isset($params['transId']) ? $params['transId'] : '';
		$response = new Response($transactionId);

		return $response;
	}

}
