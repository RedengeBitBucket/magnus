<?php

namespace Redenge\Magnus;

use InvalidArgumentException;


class Settings
{

	const
		URL_PRODUCTION = 'https://magnus.bbch.sk',
		URL_DEVELOPMENT = 'https://magnustest.axasoft.eu';

	/**
	 * @var string
	 */
	private $shopId;

	/**
	 * @var bool
	 */
	private $production;

	/**
	 * @var array
	 */
	private $pointsConversion;


	/**
	 * @param string $shopId
	 * @param bool $production
	 * @param array $pointsConversion
	 */
	public function __construct($shopId, $production, $pointsConversion)
	{
		$this->shopId = $shopId;
		$this->production = $production;
		$this->pointsConversion = $pointsConversion;
	}


	/**
	 * @return string
	 */
	public function getShopId()
	{
		return $this->shopId;
	}


	/**
	 * @return bool
	 */
	public function getProduction()
	{
		return $this->production;
	}


	/**
	 * Get point conversion for currency
	 *
	 * @param string $currency
	 * @return float
	 * @throws \InvalidArgumentException
	 */
	public function getPointConversion($currency)
	{
		$currency = strtolower($currency);
		if (!isset($this->pointsConversion[$currency])) {
			throw new InvalidArgumentException(
				sprintf('There is no ,,%s"" currency in the conversion table', $currency)
			);
		}

		return (float) $this->pointsConversion[$currency];
	}


	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->production ? self::URL_PRODUCTION : self::URL_DEVELOPMENT;
	}

}
