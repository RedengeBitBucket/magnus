<?php

namespace Redenge\Magnus;

use Redenge\Magnus\Exceptions\InvalidArgumentException;


class Operation
{

	/**
	 * @var string
	 */
	private $vs;

	/**
	 * @var float
	 */
	private $amount;

	/**
	 * @var string $currency
	 */
	private $currency;


	/**
	 * @param string $vs
	 * @param float $amount
	 * @param string $currency
	 */
	public function __construct($vs, $amount, $currency)
	{
		$this->setVs($vs);
		$this->setAmount($amount);
		$this->setCurrency($currency);
	}


	/**
	 * @param string $vs
	 */
	private function setVs($vs)
	{
		$this->vs = $vs;
	}


	/**
	 * @param int|float $amount
	 * @return $this
	 * @throws InvalidArgumentException
	 */
	private function setAmount($amount)
	{
		if (!is_int($amount)) {
			if (!is_float($amount)) {
				throw new InvalidArgumentException('AMOUNT must by type of INT or FLOAT !' . gettype($amount) . ' given');
			}
		}

		$this->amount = (float) $amount;

		return $this;
	}


	/**
	 * @param string $currency
	 * @throws InvalidArgumentException
	 */
	private function setCurrency($currency)
	{
		if (strlen($currency) > 3) {
			throw new InvalidArgumentException('CURRENCY code max. length is 3! ' . strlen($currency) . ' given');
		}

		$this->currency = $currency;
	}


	/**
	 * @return string
	 */
	public function getVs()
	{
		return $this->vs;
	}


	/**
	 * @return float
	 */
	public function getAmount()
	{
		return $this->amount;
	}


	/**
	 * @return string
	 */
	public function getCurrency()
	{
		return $this->currency;
	}


	/**
	 * Převede sumu objednávky na magnus body
	 *
	 * @param float $amount suma objednávky
	 * @param float $multiple násobek pro danou měnu
	 * @return float
	 */
	public function getPointsAmount($amount, $multiple)
	{
		return $amount * $multiple;
	}

}
