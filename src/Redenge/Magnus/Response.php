<?php

namespace Redenge\Magnus;


/**
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Response
{

	/**
	 * @var string
	 */
	private $transactionId;


	/**
	 * @param string $transactionId
	 */
	public function __construct($transactionId)
	{
		$this->transactionId = $transactionId;
	}


	/**
	 * @return string
	 */
	public function getTransactionId()
	{
		return $this->transactionId;
	}

}
