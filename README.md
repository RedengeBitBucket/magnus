# Magnus payments
Magnus is a Nette Add-On

## Requirements
Redenge/Magnus requires PHP 5.4 or higher.

- [nette/di](https://github.com/nette/di)
- [nette/application](https://github.com/nette/application)
- [nette/utils](https://github.com/nette/utils)


## Installation
The best way to install Redenge/Magnus is using  [Composer](http://getcomposer.org/):
1) You must add repository field to your composer.json
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Require magnus package
```sh
$ composer require redenge/magnus
```
3) Enable the extension using your neon config
```yml
extensions:
	magnus: Redenge\Magnus\DI\MagnusExtension
```
4) Neon setting
```yml
magnus:
	settings:
		- 
			key: Redenge\Magnus\ProviderKeyFactory::create(<multishop>, <country>) # or any string 
        	shopId: <your shopId>
        	production: <True if you want call production environment or False call development environment>
    pointsConversion:
    	czk: 4 <How much points is 1 CZK>
    	eur: 100 <How much points is 1 EUR>
```

## Usage
```php
use Redenge\Magnus\Exceptions\MagnusException;
use Redenge\Magnus\Request;
use Redenge\Magnus\Response;
use Redenge\Magnus\Operation;
use Redenge\Magnus\Transaction;

class MyPresenter extends Nette\Application\UI\Presenter
{

	/** @var \Redenge\Magnus\Components\MagnusControlFactory @inject */
	public $magnusFactory;

	/**
     * @return MagnusControl
     * @throws InvalidArgumentException
     */
    public function createComponentWebPayButton()
    {
        $operation = new Operation(string $vs, float $amount, int $curencyCode);

        $control = $this->magnusFactory->create($operation);

        # Run before redirect to gateway
        $control->onCheckout[] = function (MagnusControl $control, Request $request, Transaction $transaction){

            //...

        }


        # On success response
        $control->onSuccess[] = function(MagnusControl $control, Response $response) {

            //....

        };

        # On Error
        $control->onError[] = function(MagnusControl $control, MagnusException $exception)
        {

            //...

        };

        return $control;

    }
}
```

## Templates

```smarty
{var $attrs = array(class => 'btn btn-primary')}
{control webPayButton $attrs, 'text on button'}
```
